 $(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
          interval: 2000
        });
        $("#contacto").on('show.bs.modal', function (e){
            console.log('Se está mostrando');
            $('#contactoBtn').removeClass('btn-outline-success');
            $('#contactoBtn').addClass('btn-primary');
            $('#contactoBtn').prop('disabled', true);
        });
        $("#contacto").on('shown.bs.modal', function (e){
            console.log('Se mostró');
        });
        $("#contacto").on('hide.bs.modal', function (e){
            console.log('Se oculta');
        });
        $("#contacto").on('hidden.bs.modal', function (e){
            console.log('Se ocultó');
            $('#contactoBtn').prop('disabled', false);
            $('#contactoBtn').removeClass('btn-primary');
            $('#contactoBtn').addClass('btn-outline-success');

        });
      }); 